﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gradusi_temperatura
{ 
    internal class Program
    {
        static void Main()
        {
            //Console.WriteLine("t, C \t \t t, F");
            //for (double x = -50; x <= 50; x = x + 1)
            //{
            //    Console.WriteLine($"{x} \t \t { Math.Round(x * 9 / 5 + 32, 2)}");
            //}

            for (int i = -50; i <= 50; i++)
            {
                double far = 9 / 5 * i + 32;

                Console.WriteLine("Температура {0}C", far);
            }
            
          Console.ReadKey();
        
        }
    }
}
    
